import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RmiServer implements RmiServerIF {


    public MedicationPlan getMedicationPlan() throws Exception {


        String driverName = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/medication";
        String userName = "root";
        String password = "rihanna";

        Connection conn = null;
        Statement stmt = null;


        //Register JDBC driver
        Class.forName("com.mysql.jdbc.Driver");

        //Open a connection
        System.out.println("Connecting to a selected database...");
        conn = DriverManager.getConnection(url, userName, password);
        System.out.println("Connected database successfully...");


        MedicationPlan medicationPlan = new MedicationPlan();
        stmt = conn.createStatement();
        String sql = "SELECT * FROM medication_plan";
        ResultSet rs = stmt.executeQuery(sql);
        //Extract data from result set
        while (rs.next()) {
            // Retrieve by column name
            int id = rs.getInt("id");
            int doctor = rs.getInt("doctor");
            String intake_intervals = rs.getString("intake_intervals");
            int patient = rs.getInt("patient");
            String start_plan = rs.getString("start_plan");
            String stop_plan = rs.getString("stop_plan");


            // Setting the values
            medicationPlan.setId(id);
            medicationPlan.setIntake_intervals(intake_intervals);
            medicationPlan.setStart_plan(start_plan);
            medicationPlan.setStop_plan(stop_plan);
            medicationPlan.setDoctor(doctor);
            medicationPlan.setPatient(patient);

          //  list.add(medicationPlan);

        }


        rs.close();
        return medicationPlan;



    }

    @Override
    public ArrayList<MedicationPlan> getList() throws Exception {
        ArrayList<MedicationPlan> list= new ArrayList<MedicationPlan>();


        String driverName = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/medication";
        String userName = "root";
        String password = "rihanna";

        Connection conn = null;
        Statement stmt = null;


        //Register JDBC driver
        Class.forName("com.mysql.jdbc.Driver");

        //Open a connection
        System.out.println("Connecting to a selected database...");
        conn = DriverManager.getConnection(url, userName, password);
        System.out.println("Connected database successfully...");



        stmt = conn.createStatement();
        String sql = "SELECT * FROM medication_plan";
        ResultSet rs = stmt.executeQuery(sql);
        //Extract data from result set
        while (rs.next()) {

            // Retrieve by column name
            int id = rs.getInt("id");
            int doctor = rs.getInt("doctor");
            String intake_intervals = rs.getString("intake_intervals");
            int patient = rs.getInt("patient");
            String start_plan = rs.getString("start_plan");
            String stop_plan = rs.getString("stop_plan");

            MedicationPlan medicationPlan = new MedicationPlan(id,intake_intervals,start_plan,stop_plan,doctor,patient);
            list.add(medicationPlan);

        }

        //list.add(medicationPlan);
        rs.close();
        return list;

    }


}
