import java.rmi.Remote;
import java.util.ArrayList;
import java.util.List;

public interface RmiServerIF extends Remote {

    MedicationPlan getMedicationPlan() throws Exception;
    ArrayList<MedicationPlan> getList() throws Exception;
}
