import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Main extends RmiServer {
    public Main() {
    }

    public static void main(String[] args) throws Exception {


        try {
            // Instantiating the implementation class
            RmiServer obj = new RmiServer();

            // Exporting the object of implementation class (
            //here we are exporting the remote object to the stub)
            RmiServerIF stub = (RmiServerIF) UnicastRemoteObject.exportObject(obj, 0);

            // Binding the remote object (stub) in the registry
            Registry registry = LocateRegistry.getRegistry();

            registry.bind("RmiServer ", stub);
            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }

    }


}
