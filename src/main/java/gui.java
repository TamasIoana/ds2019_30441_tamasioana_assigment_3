

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.*;
import java.awt.event.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class gui implements ActionListener {
    JFrame frame, frame1;
    JButton button;
    static JTable table;
    // MedicationPlan medicationPlan;

    String[] columnNames;
    ArrayList<MedicationPlan> list;
    MedicationPlan medicationPlan;
    DefaultTableModel model = new DefaultTableModel();
    Object[] o = new Object[6];

    public gui(String[] columnNames, ArrayList<MedicationPlan> list) {

        this.columnNames = columnNames;
        //this.medicationPlan = medicationPlan;
        // this.medicationPlan=medicationPlan;
        this.list = list;
    }

    public void createUI() throws Exception{
        Calendar now = Calendar.getInstance();
        final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        final JLabel tijd = new JLabel(dateFormat.format(now.getTime()));

        frame = new JFrame("Database Search Result");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        button = new JButton("Display Medication Plan");
        button.setBounds(120, 130, 400, 20);
        button.addActionListener(this);
        frame.add(button);
        frame.setVisible(true);
        frame.setSize(500, 400);


        tijd.setBounds(100, 100, 125, 125);
        frame.add(tijd);
        new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Calendar now = Calendar.getInstance();
                Date start = null;
                tijd.setText(dateFormat.format(now.getTime()));
                SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");

                DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                String mytime = "13:48:10";
                try {
                    start = sdf.parse(mytime);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }

                if ((sdf.format(start)).equals(sdf.format(now.getTime()))) {

                    //showTableData();

                    //System.out.println("im here");
//                Object[] o = new Object[6];
//                o[0]=medicationPlan.getId();
//                o[1]=medicationPlan.getDoctor();
//                o[2]=medicationPlan.getIntake_intervals();
//                o[3]=medicationPlan.getPatient();
//                o[4]=medicationPlan.getStart_plan();
//                o[5]=medicationPlan.getStop_plan();
//                model.addRow(o);

                    // Getting the registry
                    Registry registry = null;
                    try {
                        registry = LocateRegistry.getRegistry(null);
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    }

                    // Looking up the registry for the remote object
                    RmiServerIF stub = null;
                    try {
                        stub = (RmiServerIF) registry.lookup("RmiServer ");
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    } catch (NotBoundException e1) {
                        e1.printStackTrace();
                    }


                    // Calling the remote method using the obtained object
                    // MedicationPlan medicationPlan = stub.getMedicationPlan();

                    ArrayList<MedicationPlan> medicationPlans1 = null;
                    try {
                        medicationPlans1 = stub.getList();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    model.getDataVector().removeAllElements();

                        for (int i = 0; i < medicationPlans1.size(); i++) {

                            o[0] = medicationPlans1.get(i).getId();
                            o[1] = medicationPlans1.get(i).getDoctor();
                            o[2] = medicationPlans1.get(i).getIntake_intervals();
                            o[3] = medicationPlans1.get(i).getPatient();
                            o[4] = medicationPlans1.get(i).getStart_plan();
                            o[5] = medicationPlans1.get(i).getStop_plan();
                            model.addRow(o);

                        }


                } else {
                    System.out.println("wait");
//                System.out.println(sdf.format(start));
//                System.out.println(sdf.format(now.getTime()));


                }

            }
        }).start();


    }

    public void actionPerformed(ActionEvent ae) {
        button = (JButton) ae.getSource();
        System.out.println("Showing Table Data.......");
        try {
            showTableData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showTableData() throws SQLException {

        frame1 = new JFrame("Database Search Result");
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame1.setLayout(new BorderLayout());


        model.setColumnIdentifiers(columnNames);
        table = new JTable();
        table.setModel(model);

        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setFillsViewportHeight(true);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setHorizontalScrollBarPolicy(
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        //for (MedicationPlan m : list){
        for (int i = 0; i < list.size(); i++) {
            //Object[] o = new Object[6];
            o[0] = list.get(i).getId();
            o[1] = list.get(i).getDoctor();
            o[2] = list.get(i).getIntake_intervals();
            o[3] = list.get(i).getPatient();
            o[4] = list.get(i).getStart_plan();
            o[5] = list.get(i).getStop_plan();
            model.addRow(o);
//        Object[] o = new Object[6];
//                o[0]=medicationPlan.getId();
//                o[1]=medicationPlan.getDoctor();
//                o[2]=medicationPlan.getIntake_intervals();
//                o[3]=medicationPlan.getPatient();
//                o[4]=medicationPlan.getStart_plan();
//                o[5]=medicationPlan.getStop_plan();
//                model.addRow(o);


        }
        frame1.add(scroll);
        frame1.setVisible(true);
        frame1.setSize(400, 300);
    }


}


