import java.io.Serializable;

public class MedicationPlan implements Serializable {

    private Integer id;
    private String intake_intervals;
    private String start_plan;
    private String stop_plan;
    private Integer doctor;
    private Integer patient;

    public MedicationPlan(Integer id, String intake_intervals, String start_plan, String stop_plan, Integer doctor, Integer patient) {
        this.id = id;
        this.intake_intervals = intake_intervals;
        this.start_plan = start_plan;
        this.stop_plan = stop_plan;
        this.doctor = doctor;
        this.patient = patient;
    }

    public MedicationPlan() {
        
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntake_intervals() {
        return intake_intervals;
    }

    public void setIntake_intervals(String intake_intervals) {
        this.intake_intervals = intake_intervals;
    }

    public String getStart_plan() {
        return start_plan;
    }

    public void setStart_plan(String start_plan) {
        this.start_plan = start_plan;
    }

    public String getStop_plan() {
        return stop_plan;
    }

    public void setStop_plan(String stop_plan) {
        this.stop_plan = stop_plan;
    }

    public Integer getDoctor() {
        return doctor;
    }

    public void setDoctor(Integer doctor) {
        this.doctor = doctor;
    }

    public Integer getPatient() {
        return patient;
    }

    public void setPatient(Integer patient) {
        this.patient = patient;
    }

}